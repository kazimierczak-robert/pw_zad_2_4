// Zad 2.4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdlib>
#include <time.h>
#include <windows.h>
#define numberOfThreads 4

CRITICAL_SECTION criticalSection;
int *tab;
volatile int* histogram;
int n = 0;

DWORD WINAPI ThreadFunc_SUMA(LPVOID lpdwParam)
{
	int in = (int)lpdwParam;
	int wielkosc_porcji;
	n % numberOfThreads >= numberOfThreads / 2 ? wielkosc_porcji = ((n / numberOfThreads) + 1) : wielkosc_porcji = n / numberOfThreads;
	int pocz = in * wielkosc_porcji;
	int kon;
	(in == numberOfThreads - 1) ? kon = n : kon = pocz + wielkosc_porcji;

	int temp_sum = 0;
	for (int i = pocz; i < kon; i++)
	{
		EnterCriticalSection(&criticalSection);
		histogram[tab[i]] += 1;
		LeaveCriticalSection(&criticalSection);
	}
	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	n = 10;
	tab = new int[n];
	for (int i = 0; i < n; i++)
	{
		tab[i] = rand() % 10;
	}
	for (int i = 0; i < n; i++)
	{
		printf("%d ", tab[i]);
	}
	printf("\n");

	histogram = new int[10];
	for (int i = 0; i < 10; i++)
	{
		histogram[i] = 0;
	}

	InitializeCriticalSection(&criticalSection);
	HANDLE ahThread[numberOfThreads];

	int *tab_index = new int[numberOfThreads];
	for (int i = 0; i < numberOfThreads; i++)
	{
		tab_index[i] = i;
	}
	for (int i = 0; i < numberOfThreads; i++)
	{
		ahThread[i] = CreateThread(NULL, 0, ThreadFunc_SUMA, LPVOID(tab_index[i]), 0, NULL);
	}
	for (int i = 0; i < numberOfThreads; i++)
	{
		WaitForSingleObject(ahThread[i], INFINITE);
	}
	for (int i = 0; i < numberOfThreads; i++)
	{
		CloseHandle(ahThread[i]);
	}
	DeleteCriticalSection(&criticalSection);

	printf("HISTOGRAM\n");
	for (int i = 0; i < 10; i++)
	{
		printf("%d ",histogram[i]);
	}
	printf("\n");
	system("pause");
	return 0;
}

